import archipelagos.client.*;
import archipelagos.common.data.*;
import archipelagos.common.data.collection.*;
import archipelagos.common.data.collection.Collection;
import archipelagos.common.platform.*;

import com.google.gson.*;

import java.util.*;

/**
 * Examples of how to use the Archipelagos Java Client to retrieve and update Collections.
 */
public class CollectionExamples
{
	private static final String APIKEY = "test_user12345";

	private static final String ISSUED = "Issued";
	private static final String EFFECTIVE = "Effective";
	private static final String MATURITY = "Maturity";
	private static final String COUPON = "Coupon";
	private static final String SECTOR = "Sector";

	private static final String BOND_SOURCE = "ARCHI";
	private static final String BOND_CATEGORY = "TRY";
	private static final String BOND_LABEL = "STATIC";
	private static final List<String> BOND_FEATURES = List.of(ISSUED, EFFECTIVE, MATURITY, COUPON, SECTOR);

	private static final int MAX_SIZE = 1_000;


	/**
	 * Performs a time-series metadata request for the bond collection using the Client API.
	 */
	public static void bondCollectionMetadataApiRequest() throws Exception
	{
		// Build a time-series request and send it to the platform

		CollectionMetadataRequest request = CollectionMetadataRequest.of(BOND_SOURCE, BOND_CATEGORY, BOND_LABEL);
		CollectionMetadataResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata for the collection

			CollectionMetadata metadata = response.getMetadata();


			// Display the data returned

			StringBuilder dataAsString = new StringBuilder();
			dataAsString.append(String.format("Source: %s\n", metadata.getSource()));
			dataAsString.append(String.format("Category: %s\n", metadata.getCategory()));
			dataAsString.append(String.format("Label: %s\n", metadata.getLabel()));
			dataAsString.append(String.format("URL: %s\n", metadata.getUrl()));
			dataAsString.append(String.format("Summary: %s\n", metadata.getSummary()));
			dataAsString.append(String.format("Description: %s\n", metadata.getDescription()));
			dataAsString.append("Features: ");
			boolean isFirst = true;

			for (Map.Entry<String, String> feature : metadata.getFeatures().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"");
				dataAsString.append(feature.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"");
				dataAsString.append(feature.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append("\nProperties: ");
			isFirst = true;

			for (Map.Entry<String, String> property : metadata.getProperties().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"");
				dataAsString.append(property.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"");
				dataAsString.append(property.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append(String.format("\nCreated: %s\n", DataUtils.getDateTime(metadata.getCreated())));
			dataAsString.append(String.format("Edited: %s\n", DataUtils.getDateTime(metadata.getEdited())));
			dataAsString.append(String.format("Refreshed: %s\n", DataUtils.getDateTime(metadata.getRefreshed())));

			System.out.printf("The platform returned the following metadata for the bond collection %s/%s/%s via the API:\n\n%s\n", metadata.getSource(), metadata.getCategory(), metadata.getLabel(), dataAsString);
		}
	}


	/**
	 * Performs a request for the bond collection using the Client API.
	 */
	public static void bondCollectionApiRequest() throws Exception
	{
		// Specify the filters

		Map<String, String> filters = Map.of(COUPON, "> 2.1", SECTOR, "= '30Y'");


		// Build a time-series request and send it to the platform

		CollectionRequest request = CollectionRequest.of(BOND_SOURCE, BOND_CATEGORY, BOND_LABEL, BOND_FEATURES, MAX_SIZE, filters);
		CollectionResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			Collection collection = response.getCollection();
			CollectionMetadata metadata = collection.getMetadata();

			CollectionData collectionData = collection.getData();
			Map<String, Map<String, Object>> data = collectionData.getData();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (String id : data.keySet())
			{
				Map<String, ?> staticData = data.get(id);

				if (staticData.size() > 0)
				{
					Date issued = (Date) staticData.get(ISSUED);
					Date effective = (Date) staticData.get(EFFECTIVE);
					Date maturity = (Date) staticData.get(MATURITY);
					double coupon = (double) staticData.get(COUPON);
					String sector = (String) staticData.get(SECTOR);

					dataAsString.append(String.format("ID: %s\t\tIssued: %s\t\tEffective: %s\t\tMaturity: %s\t\tCoupon: %,.2f\t\tSector: %s\n", id, issued, effective, maturity, coupon, sector));
				}
				else
				{
					dataAsString.append(String.format("ID: %sf\n", id));
				}
			}


			// Display the data returned

			String metadataSource = metadata.getSource();
			String metadataCategory = metadata.getCategory();
			String metadataLabel = metadata.getLabel();
			System.out.printf("The platform returned the following data for the bond collection %s/%s/%s via the API:\n\n%s\n", metadataSource, metadataCategory, metadataLabel, dataAsString);
		}
	}


	/**
	 * Performs a metadata request for the bond collection using HTTP.
	 */
	public static void bondCollectionMetadataHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.COLLECTION_METADATA_REQUEST,
				HttpConstants.SOURCE_PARAMETER, BOND_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, BOND_CATEGORY,
				HttpConstants.LABEL_PARAMETER, BOND_LABEL,
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON);
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			JsonObject metadata = json.get("metadata").getAsJsonObject();
			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();


			// Display the data returned

			StringBuilder dataAsString = new StringBuilder();
			dataAsString.append(String.format("Source: %s\n", source));
			dataAsString.append(String.format("Category: %s\n", category));
			dataAsString.append(String.format("Label: %s\n", label));
			dataAsString.append(String.format("URL: %s\n", metadata.get("url").toString()));
			dataAsString.append(String.format("Summary: %s\n", metadata.get("summary").toString()));
			dataAsString.append(String.format("Description: %s\n", metadata.get("description").toString()));
			dataAsString.append("Features: ");
			boolean isFirst = true;

			for (Map.Entry<String, JsonElement> feature : metadata.get("features").getAsJsonObject().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"");
				dataAsString.append(feature.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"");
				dataAsString.append(feature.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append("\nProperties: ");
			isFirst = true;

			for (Map.Entry<String, JsonElement> property : metadata.get("properties").getAsJsonObject().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"");
				dataAsString.append(property.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"");
				dataAsString.append(property.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append(String.format("\nCreated: %s\n", metadata.get("created").toString()));
			dataAsString.append(String.format("Edited: %s\n", metadata.get("edited").toString()));
			dataAsString.append(String.format("Refreshed: %s\n", metadata.get("refreshed").toString()));

			System.out.printf("The platform returned the following metadata for the bond collection %s/%s/%s via HTTP:\n\n%s\n", source, category, label, dataAsString);
		}
	}


	/**
	 * Performs a request for the bond collection using HTTP.
	 */
	public static void bondCollectionHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.COLLECTION_REQUEST,
				HttpConstants.SOURCE_PARAMETER, BOND_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, BOND_CATEGORY,
				HttpConstants.LABEL_PARAMETER, BOND_LABEL,
				HttpConstants.FEATURES_PARAMETER, DataUtils.getList(BOND_FEATURES),
				HttpConstants.MAX_SIZE_PARAMETER, Integer.toString(MAX_SIZE),
				HttpConstants.FLATTEN_PARAMETER, Boolean.TRUE.toString(),
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON,
				COUPON, "> 2.1",
				SECTOR, "= '30Y'");
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			JsonObject metadata = json.get("metadata").getAsJsonObject();
			JsonArray data = json.get("data").getAsJsonArray();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (JsonElement dataElement : data)
			{
				JsonObject staticData = (JsonObject) dataElement;

				if (staticData.size() > 0)
				{
					String issued = staticData.get(ISSUED).getAsString();
					String effective = staticData.get(EFFECTIVE).getAsString();
					String maturity = staticData.get(MATURITY).getAsString();
					double coupon = staticData.get(COUPON).getAsDouble();
					String sector = staticData.get(SECTOR).getAsString();

					dataAsString.append(String.format("Issued: %s\t\tEffective: %s\t\tMaturity: %s\t\tCoupon: %,.2f\t\tSector: %s\n", issued, effective, maturity, coupon, sector));
				}
			}


			// Display the data

			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();
			System.out.printf("The platform returned the following data for the bond collection %s/%s/%s via HTTP:\n\n%s\n", source, category, label, dataAsString);
		}
	}


	/**
	 *
	 */
	public static void main(String... args)
	{
		try
		{
			// An API Key can be provided for all requests as below, or it can be provided (overridden) in individual requests

			Archipelagos.API_KEY = APIKEY;


			// Send collection requests via the Client API

			bondCollectionMetadataApiRequest();
			bondCollectionApiRequest();


			// Send collection requests via HTTP

			bondCollectionMetadataHttpRequest();
			bondCollectionHttpRequest();
		}
		catch (Exception e)
		{
			System.out.printf("An exception was thrown when running the application.\n\n%s", e);
			System.exit(1);
		}
	}
}
