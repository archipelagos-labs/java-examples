import archipelagos.client.*;
import archipelagos.common.data.*;
import archipelagos.common.data.timeseries.*;
import archipelagos.common.platform.*;

import com.google.gson.*;

import java.nio.charset.StandardCharsets;
import java.time.*;
import java.util.*;

/**
 * Examples of how to use the Archipelagos Java Client to retrieve and update Time-Series.
 */
public class TimeSeriesExamples
{
	private static final String APIKEY = "test_user12345";

	private static final String PRICE_SOURCE = "ARCHI";
	private static final String PRICE_CATEGORY = "FX";
	private static final String PRICE_LABEL = "USD_JPY";
	private static final String OPEN_PRICE = "Open";
	private static final String HIGH_PRICE = "High";
	private static final String LOW_PRICE = "Low";
	private static final String CLOSE_PRICE = "Close";
	private static final List<String> PRICE_FEATURES = List.of(new String[]{OPEN_PRICE, HIGH_PRICE, LOW_PRICE, CLOSE_PRICE});

	private static final String PERSON_SOURCE = "ARCHI";
	private static final String PERSON_CATEGORY = "PERSON";
	private static final String PERSON_LABEL = "EMPLOYEE";
	private static final String NAME_OF_PERSON = "Name";
	private static final String AGE = "Age";
	private static final String PHOTO = "Photo";
	private static final String NOTES = "Notes";
	private static final String SALARY = "Notes.Salary";

	private static final LocalDateTime START = null;
	private static final LocalDateTime END = null;
	private static final int MAX_SIZE = 1_000;
	private static final boolean ASCENDING = true;


	/**
	 * Performs a time-series request for the price time-series using the Client API.
	 */
	public static void priceTimeSeriesApiRequest() throws Exception
	{
		// Specify the filters

		Map<String, String> filters = new HashMap<>();
		filters.put(OPEN_PRICE, "< Close and >= 130");


		// Build a time-series request and send it to the platform

		TimeSeriesRequest request = TimeSeriesRequest.of(PRICE_SOURCE, PRICE_CATEGORY, PRICE_LABEL, START, END, PRICE_FEATURES, MAX_SIZE, ASCENDING, filters);
		TimeSeriesResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			TimeSeries timeSeries = response.getTimeSeries();
			TimeSeriesMetadata timeSeriesMetadata = timeSeries.getMetadata();

			TimeSeriesData timeSeriesData = timeSeries.getData();
			SortedMap<LocalDateTime, Map<String, Object>> data = timeSeriesData.getFlattenedData();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (LocalDateTime date : data.keySet())
			{
				Map<String, ?> prices = data.get(date);

				if (prices.size() > 0)
				{
					Object open = prices.get(OPEN_PRICE);
					Object high = prices.get(HIGH_PRICE);
					Object low = prices.get(LOW_PRICE);
					Object close = prices.get(CLOSE_PRICE);

					dataAsString.append(String.format("Date: %s\t\tOpen: %s\t\tHigh: %s\t\tLow: %s\t\tClose: %s\n", date.toLocalDate(), open, high, low, close));
				}
				else
				{
					dataAsString.append(String.format("Date: %sf\n", date.toLocalDate()));
				}
			}


			// Display the data returned

			String metadataSource = timeSeriesMetadata.getSource();
			String metadataCategory = timeSeriesMetadata.getCategory();
			String metadataLabel = timeSeriesMetadata.getLabel();
			System.out.printf("The platform returned the following data for the price time-series %s/%s/%s via the API:\n\n%s\n", metadataSource, metadataCategory, metadataLabel, dataAsString);
		}
	}


	/**
	 * Performs a time-series request for the person time-series using the Client API.
	 */
	public static void personTimeSeriesApiRequest() throws Exception
	{
		// Specify the filters

		Map<String, String> filters = new HashMap<>();
		filters.put(SALARY, "(> 70_000 and < 80_000) or (> 30_000 and < 40_000)");
		filters.put(AGE, "> 20 and < 50");


		// Build a time-series request (specifying an Api Key) and send it to the platform

		TimeSeriesRequest request = TimeSeriesRequest.of(PERSON_SOURCE, PERSON_CATEGORY, PERSON_LABEL, START, END, null, MAX_SIZE, ASCENDING, filters, APIKEY);
		TimeSeriesResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			TimeSeries timeSeries = response.getTimeSeries();
			TimeSeriesMetadata timeSeriesMetadata = timeSeries.getMetadata();

			TimeSeriesData timeSeriesData = timeSeries.getData();
			SortedMap<LocalDateTime, List<Map<String, Object>>> data = timeSeriesData.getData();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (LocalDateTime date : data.keySet())
			{
				for (Map<String, ?> person : data.get(date))
				{
					if (person.size() > 0)
					{
						String name = (String) person.get(NAME_OF_PERSON);
						long age = (long) person.get(AGE);
						byte[] photo = (byte[]) person.get(PHOTO);
						Object notes = person.get(NOTES);

						dataAsString.append(String.format("Date: %s\t\tName: %s\t\t\tAge: %s\t\tPhoto: %s\t\tNotes: %s\t\t\n", date.toLocalDate(), name, age, Arrays.toString(photo), notes));
					}
					else
					{
						dataAsString.append(String.format("Date: %sf\n", date.toLocalDate()));
					}
				}
			}


			// Display the data returned

			String metadataSource = timeSeriesMetadata.getSource();
			String metadataCategory = timeSeriesMetadata.getCategory();
			String metadataLabel = timeSeriesMetadata.getLabel();
			System.out.printf("The platform returned the following data for the person time-series %s/%s/%s via the API:\n\n%s\n", metadataSource, metadataCategory, metadataLabel, dataAsString);
		}
	}


	/**
	 * Performs a time-series request for the price time-series using HTTP.
	 */
	public static void priceTimeSeriesHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.TIME_SERIES_REQUEST,
				HttpConstants.SOURCE_PARAMETER, PRICE_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, PRICE_CATEGORY,
				HttpConstants.LABEL_PARAMETER, PRICE_LABEL,
				HttpConstants.FEATURES_PARAMETER, DataUtils.getList(PRICE_FEATURES),
				HttpConstants.MAX_SIZE_PARAMETER, Integer.toString(MAX_SIZE),
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON,
				OPEN_PRICE, "< Close and >= 130");
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			JsonObject metadata = json.get("metadata").getAsJsonObject();
			JsonArray data = json.get("data").getAsJsonArray();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (JsonElement entry : data)
			{
				JsonArray tuple = entry.getAsJsonArray();
				LocalDateTime date = DataUtils.parseDateTime(tuple.get(0).getAsString());
				JsonArray featureMapList = tuple.get(1).getAsJsonArray();

				for (JsonElement featureMap : featureMapList)
				{
					JsonObject prices = (JsonObject) featureMap;

					if (prices.size() > 0)
					{
						double open = prices.get(OPEN_PRICE).getAsDouble();
						double high = prices.get(HIGH_PRICE).getAsDouble();
						double low = prices.get(LOW_PRICE).getAsDouble();
						double close = prices.get(CLOSE_PRICE).getAsDouble();

						dataAsString.append(String.format("Date: %s\t\tOpen: %,.2f\t\tHigh: %,.2f\t\tLow: %,.2f\t\tClose: %,.2f\n", date.toLocalDate(), open, high, low, close));
					}
					else
					{
						dataAsString.append(String.format("Date: %sf\n", date.toLocalDate()));
					}
				}
			}


			// Display the data

			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();
			System.out.printf("The platform returned the following data for the price time-series %s/%s/%s via HTTP:\n\n%s\n", source, category, label, dataAsString);
		}
	}


	/**
	 * Performs a time-series request for the person time-series using HTTP.
	 */
	public static void personTimeSeriesHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.TIME_SERIES_REQUEST,
				HttpConstants.SOURCE_PARAMETER, PERSON_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, PERSON_CATEGORY,
				HttpConstants.LABEL_PARAMETER, PERSON_LABEL,
				HttpConstants.MAX_SIZE_PARAMETER, Integer.toString(MAX_SIZE),
				HttpConstants.API_KEY_PARAMETER, APIKEY,
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON,
				SALARY, "> 70_000 and < 80_000 or > 30_000 and < 40_000",
				AGE, "> 20 and < 50");
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the time-series returned

			JsonObject metadata = json.get("metadata").getAsJsonObject();
			JsonArray data = json.get("data").getAsJsonArray();


			// Gather the data returned

			StringBuilder dataAsString = new StringBuilder();

			for (JsonElement entry : data)
			{
				JsonArray tuple = entry.getAsJsonArray();
				LocalDateTime date = DataUtils.parseDateTime(tuple.get(0).getAsString());
				JsonArray featureMapList = tuple.get(1).getAsJsonArray();

				for (JsonElement featureMap : featureMapList)
				{
					JsonObject person = (JsonObject) featureMap;

					if (person.size() > 0)
					{
						String name = person.get(NAME_OF_PERSON).getAsString();
						long age = person.get(AGE).getAsInt();
						byte[] photo = person.get(PHOTO).getAsString().getBytes(StandardCharsets.UTF_8);
						JsonObject notes = person.get(NOTES).getAsJsonObject();

						dataAsString.append(String.format("Date: %s\t\tName: %s\t\t\tAge: %s\t\tPhoto: %s\t\tNotes: %s\t\t\n", date.toLocalDate(), name, age, Arrays.toString(photo), notes));
					}
					else
					{
						dataAsString.append(String.format("Date: %sf\n", date.toLocalDate()));
					}
				}
			}


			// Display the data returned

			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();
			System.out.printf("The platform returned the following data for the person time-series %s/%s/%s via HTTP:\n\n%s\n", source, category, label, dataAsString);
		}
	}


	/**
	 *
	 */
	public static void main(String... args)
	{
		try
		{
			// An API Key can be provided for all requests as below, or it can be provided (overridden)
			// in individual requests; for an example of this please see personTimeSeriesHttpRequest()

			Archipelagos.API_KEY = APIKEY;


			// Send time-series requests via the Client API

			priceTimeSeriesApiRequest();
			personTimeSeriesApiRequest();


			// Send time-series requests via HTTP

			priceTimeSeriesHttpRequest();
			personTimeSeriesHttpRequest();
		}
		catch (Exception e)
		{
			System.out.printf("An exception was thrown when running the application.\n\n%s", e);
			System.exit(1);
		}
	}
}
