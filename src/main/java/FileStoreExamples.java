import archipelagos.client.*;
import archipelagos.common.data.*;
import archipelagos.common.data.filestore.*;
import archipelagos.common.platform.HttpConstants;

import com.google.gson.*;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Examples of how to use the Archipelagos Java Client to retrieve and update File Stores.
 */
public class FileStoreExamples
{
	private static final String APIKEY = "test_user12345";

	private static final String US_CB_SOURCE = "ARCHI";
	private static final String US_CB_CATEGORY = "USCB";
	private static final String US_CB_LABEL = "STATE";

	private static final String POPULATION_FILE_NAME = "Population.csv";


	/**
	 * Performs a file store metadata request for the US Census Bureau file store using the Client API.
	 */
	public static void fileStoreMetadataApiRequest() throws Exception
	{
		// Build a request and send it to the platform

		FileStoreMetadataRequest request = FileStoreMetadataRequest.of(US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL);
		FileStoreMetadataResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata for the collection

			FileStoreMetadata metadata = response.getMetadata();


			// Display the data returned

			StringBuilder dataAsString = new StringBuilder();
			dataAsString.append(String.format("Source: %s\n", metadata.getSource()));
			dataAsString.append(String.format("Code: %s\n", metadata.getCategory()));
			dataAsString.append(String.format("Label: %s\n", metadata.getLabel()));
			dataAsString.append(String.format("URL: %s\n", metadata.getUrl()));
			dataAsString.append(String.format("Summary: %s\n", metadata.getSummary()));
			dataAsString.append(String.format("Description: %s\n", metadata.getDescription()));
			dataAsString.append("Properties: ");
			boolean isFirst = true;

			for (Map.Entry<String, String> property : metadata.getProperties().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"" );
				dataAsString.append(property.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"" );
				dataAsString.append(property.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append(String.format("\nPremium: %s\n", metadata.isPremium()));
			dataAsString.append(String.format("Created: %s\n", DataUtils.getDateTime(metadata.getCreated())));
			dataAsString.append(String.format("Edited: %s\n", DataUtils.getDateTime(metadata.getEdited())));

			System.out.printf("The platform returned the following metadata for the US Census Bureau file store %s/%s/%s via the API:\n\n%s\n", metadata.getSource(), metadata.getCategory(), metadata.getLabel(), dataAsString);
		}
	}


	/**
	 * Performs a request for the US Census Bureau file store using the Client API.
	 */
	public static void fileStoreApiRequest() throws Exception
	{
		// Build a request and send it to the platform

		FileStoreRequest request = FileStoreRequest.of(US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL, POPULATION_FILE_NAME);
		FileStoreResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the file returned

			byte[] fileContents = response.getFile();
			String textContents = new String(fileContents, StandardCharsets.UTF_8);
			FileMetadata metadata = response.getMetadata();


			// Display the data returned

			String dataAsString = "";
			dataAsString += String.format("Source: %s\n", metadata.getSource());
			dataAsString += String.format("Code: %s\n", metadata.getCategory());
			dataAsString += String.format("Label: %s\n", metadata.getLabel());
			dataAsString += String.format("Name: %s\n", metadata.getName());
			dataAsString += String.format("Size: %s\n", metadata.getSize());
			dataAsString += String.format("Premium: %s\n", metadata.isPremium());
			dataAsString += String.format("Created: %s\n\n", DataUtils.getDateTime(metadata.getCreated()));
			dataAsString += textContents;

			System.out.printf("The platform returned the following data for the file '%s' from the file store %s/%s/%s via the API:\n\n%s\n", POPULATION_FILE_NAME, US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL, dataAsString);
		}
	}


	/**
	 * Performs a file metadata request for the US Census Bureau file store using the Client API.
	 */
	public static void fileMetadataApiRequest() throws Exception
	{
		// Build a request and send it to the platform

		FileMetadataRequest request = FileMetadataRequest.of(US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL, "");
		FileMetadataResponse response = Archipelagos.send(request);


		// Check what type of response was received

		if (response.errorOccurred())
		{
			int errorCode = response.getErrorCode();
			String errorMessage = response.getErrorMessage();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Display the data returned

			List<FileMetadata> metadata = response.getMetadata();
			StringBuilder dataAsString = new StringBuilder();

			for(FileMetadata fileMetadata : metadata)
			{
				dataAsString.append(String.format("Source: %s\t\t", fileMetadata.getSource()));
				dataAsString.append(String.format("Category: %s\t\t", fileMetadata.getCategory()));
				dataAsString.append(String.format("Label %s\t\t", fileMetadata.getLabel()));
				dataAsString.append(String.format("Name: %-18.18s\t\t", fileMetadata.getName()));
				dataAsString.append(String.format("Size: %-10.10s\t\t", fileMetadata.getSize()));
				dataAsString.append(String.format("Premium: %s\t\t", fileMetadata.isPremium()));
				dataAsString.append(String.format("Created: %s\n", DataUtils.getDateTime(fileMetadata.getCreated())));
			}

			System.out.printf("The platform returned the following file metadata for the US Census Bureau file store %s/%s/%s via the API:\n\n%s\n", US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL, dataAsString);
		}
	}


	/**
	 * Performs a file store metadata request for the US Census Bureau file store using HTTP.
	 */
	public static void fileStoreMetadataHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_STORE_METADATA_REQUEST,
				HttpConstants.SOURCE_PARAMETER, US_CB_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, US_CB_CATEGORY,
				HttpConstants.LABEL_PARAMETER, US_CB_LABEL,
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON);
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			JsonObject metadata = json.get("metadata").getAsJsonObject();
			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();


			// Display the data returned

			StringBuilder dataAsString = new StringBuilder();
			dataAsString.append(String.format("Source: %s\n", source));
			dataAsString.append(String.format("Category: %s\n", category));
			dataAsString.append(String.format("Label: %s\n", label));
			dataAsString.append(String.format("URL: %s\n", metadata.get("url").getAsString()));
			dataAsString.append(String.format("Summary: %s\n", metadata.get("summary").getAsString()));
			dataAsString.append(String.format("Description: %s\n", metadata.get("description").getAsString()));
			dataAsString.append("Properties: ");
			boolean isFirst = true;

			for (Map.Entry<String, JsonElement> property : metadata.get("properties").getAsJsonObject().entrySet())
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else
				{
					dataAsString.append(", ");
				}

				dataAsString.append("\"" );
				dataAsString.append(property.getKey());
				dataAsString.append("\"");
				dataAsString.append(" => ");
				dataAsString.append("\"" );
				dataAsString.append(property.getValue());
				dataAsString.append("\"");
			}

			dataAsString.append(String.format("\nPremium: %s\n", metadata.get("premium").getAsString()));
			dataAsString.append(String.format("Created: %s\n", metadata.get("created").getAsString()));
			dataAsString.append(String.format("Edited: %s\n", metadata.get("edited").getAsString()));

			System.out.printf("The platform returned the following metadata for the US Census Bureau file store %s/%s/%s via HTTP:\n\n%s\n", source, category, label, dataAsString);
		}
	}


	/**
	 * Performs a request for the US Census Bureau file store using HTTP.
	 */
	public static void fileStoreHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_STORE_REQUEST,
				HttpConstants.SOURCE_PARAMETER, US_CB_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, US_CB_CATEGORY,
				HttpConstants.LABEL_PARAMETER, US_CB_LABEL,
				HttpConstants.FILENAME_PARAMETER, POPULATION_FILE_NAME,
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON);
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Obtain the metadata and data for the file returned

			JsonObject metadata = json.get("metadata").getAsJsonObject();
			String textContents = json.get("file").getAsString();


			// Display the data returned

			String dataAsString = "";
			dataAsString += (String.format("Source: %s\n", metadata.get("source").getAsString()));
			dataAsString += String.format("Category: %s\n", metadata.get("category").getAsString());
			dataAsString += String.format("Label: %s\n", metadata.get("label").getAsString());
			dataAsString += String.format("Name: %s\n", metadata.get("name").getAsString());
			dataAsString += String.format("Size: %s\n", metadata.get("size").getAsString());
			dataAsString += String.format("Premium: %s\n", metadata.get("premium").getAsString());
			dataAsString += String.format("Created: %s\n\n", metadata.get("created").getAsString());
			dataAsString += textContents;


			// Display the data

			String source = metadata.get("source").getAsString();
			String category = metadata.get("category").getAsString();
			String label = metadata.get("label").getAsString();
			String fileName = metadata.get("name").getAsString();
			System.out.printf("The platform returned the following data for the file '%s' the US Census Bureau file store %s/%s/%s via HTTP:\n\n%s\n", fileName, source, category, label, dataAsString);
		}
	}


	/**
	 * Performs a file metadata request for the US Census Bureau file store using HTTP.
	 */
	public static void fileMetadataHttpRequest() throws Exception
	{
		// Build the object representing the request (an API Key can be specified, but not in this example)

		Map<String, String> params = Map.of(
				HttpConstants.TYPE_PARAMETER, HttpConstants.FILE_METADATA_REQUEST,
				HttpConstants.SOURCE_PARAMETER, US_CB_SOURCE,
				HttpConstants.CATEGORY_PARAMETER, US_CB_CATEGORY,
				HttpConstants.LABEL_PARAMETER, US_CB_LABEL,
				HttpConstants.PATTERN_PARAMETER, "",
				HttpConstants.FORMAT_PARAMETER, HttpConstants.FORMAT_JSON);
		String request = Archipelagos.buildUrl(params);


		// Send the request to the platform and inspect the response

		String response = Archipelagos.send(request);
		JsonObject json = JsonParser.parseString(response).getAsJsonObject();

		if (json.get("error_occurred").getAsBoolean())
		{
			int errorCode = json.get("error_code").getAsInt();
			String errorMessage = json.get("error_message").getAsString();

			System.out.printf("The platform returned an error code (%s) and message: \"%s\"", errorCode, errorMessage);
		}
		else
		{
			// Display the data returned

			JsonArray metadata = json.get("metadata").getAsJsonArray();
			StringBuilder dataAsString = new StringBuilder();

			for (JsonElement fileMetadata : metadata)
			{
				JsonObject fileMetadataObj = (JsonObject)fileMetadata;

				dataAsString.append(String.format("Source: %s\t\t", fileMetadataObj.get("source").getAsString()));
				dataAsString.append(String.format("Category: %s\t\t", fileMetadataObj.get("category").getAsString()));
				dataAsString.append(String.format("Label: %s\t\t", fileMetadataObj.get("label").getAsString()));
				dataAsString.append(String.format("Name: %-18.18s\t\t", fileMetadataObj.get("name").getAsString()));
				dataAsString.append(String.format("Size: %-10.10s\t\t", fileMetadataObj.get("size").getAsString()));
				dataAsString.append(String.format("Premium: %s\t\t", fileMetadataObj.get("premium").getAsString()));
				dataAsString.append(String.format("Created: %s\n", fileMetadataObj.get("created").getAsString()));
			}

			System.out.printf("The platform returned the following file metadata for the US Census Bureau file store %s/%s/%s via HTTP:\n\n%s\n", US_CB_SOURCE, US_CB_CATEGORY, US_CB_LABEL, dataAsString);
		}
	}


	/**
	 *
	 */
	public static void main(String... args)
	{
		try
		{
			// An API Key can be provided for all requests as below, or it can be provided (overridden) in individual requests

			Archipelagos.API_KEY = APIKEY;


			// Send file store requests via the Client API

			fileStoreMetadataApiRequest();
			fileStoreApiRequest();
			fileMetadataApiRequest();


			// Send file store requests via HTTP

			fileStoreMetadataHttpRequest();
			fileStoreHttpRequest();
			fileMetadataHttpRequest();
		}
		catch (Exception e)
		{
			System.out.printf("An exception was thrown when running the application.\n\n%s", e);
			System.exit(1);
		}
	}
}
